package gamp.roguelike.model.tiles;


public class Way extends AbstrTile {

	public Way(Integer x, Integer y) {
		super(x, y);
	}

	public boolean isWalkable() {
		return true;
	}

	public TileType getType() {
		return TileType.WAY;
	}
	
	public String toString() {
		return "Way";
	}

}
