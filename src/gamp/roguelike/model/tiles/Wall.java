package gamp.roguelike.model.tiles;


public class Wall extends AbstrTile{
	
	public Wall(final Integer x, final Integer y) {
		super(x, y);
	}
	
	public boolean isWalkable() {
		return false;
	}

	public TileType getType() {
		return TileType.WALL;
	}
	
	public String toString() {
		return "Wall";
	}
}
