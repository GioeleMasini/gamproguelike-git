package gamp.roguelike.model.tiles;

import com.badlogic.gdx.math.Vector2;

public abstract class AbstrTile implements ITile {
	private final Vector2 position;

	public AbstrTile(Integer x, Integer y) {
		this.position = new Vector2(x, y);
	}

	@Override
	public Vector2 getPosition() {
		return this.position;
	}
}
