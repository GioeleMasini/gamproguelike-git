package gamp.roguelike.model.tiles;

import com.badlogic.gdx.math.Vector2;

public interface ITile {
	public Vector2 getPosition();
	public boolean isWalkable();
	public TileType getType();
}
