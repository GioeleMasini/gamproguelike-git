package gamp.roguelike.model.map;

import gamp.roguelike.controller.ModelController;
import gamp.roguelike.model.objects.IObject;
import gamp.roguelike.model.objects.creatures.ICreature;
import gamp.roguelike.model.tiles.ITile;
import gamp.roguelike.model.tiles.Wall;
import gamp.roguelike.model.tiles.Way;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.math.Vector2;

public class Map {
	private List<List<ITile>> tiles = new ArrayList<List<ITile>>();
	private List<ICreature> enemies = new ArrayList<ICreature>();
	private Vector2 bobSpawn;
	
	public Map(int width, int height) {
		this.tiles = new ArrayList<List<ITile>>(height);
		for (int y = 0; y < height; y++) {
			List<ITile> line = new ArrayList<ITile>(width);
			this.tiles.add(line);
			for (int x = 0; x < width; x++) {
				line.add(x, new Wall(x, y));
			}
		}
	}
	
	public List<List<ITile>> getTiles() {
		return this.tiles;
	}
	
	public ITile get(Integer x, Integer y) {
		try {
			return this.tiles.get(y).get(x);
		} catch (IndexOutOfBoundsException e) {
			return null;
		}
	}
	
	public void set(Integer x, Integer y, ITile newTile) {
		this.tiles.get(y).set(x, newTile);
	}
	
	public int getWidth() {
		return this.tiles.get(0).size();
	}
	
	public int getHeight() {
		return this.tiles.size();
	}
	
	public List<ICreature> getEnemies() {
		if (this.enemies == null || this.enemies.isEmpty()) {
			if (this.enemies == null) {
				this.enemies = new ArrayList<ICreature>();
			}
			for (int i = 0; i < 10; i++) {
				enemies.add(ModelController.getInstance().getNewEnemy());
			}
		}
		return this.enemies;
	}
	
	public boolean destroy(IObject o) {
		if (this.enemies.contains(o)) {
			this.enemies.remove(o);
			return true;
		}
		return false;
	}

	public boolean isOccupied(int x, int y) {
		if (this.getObjectAt(x, y) != null) {
			return true;
		}
		return false;
	}

	public IObject getObjectAt(int x, int y) {
		Vector2 pos = new Vector2(x, y);
		for (IObject o : this.enemies) {
			if (o.getPosition().equals(pos)) {
				return o;
			}
		}
		return null;
	}
	
	/**
	 * Crea una stanza di una certa dimensione a partire da una certa posizione
	 * 
	 * @param width
	 * @param height
	 * @param x
	 * @param y
	 */
	public void addRoom(int width, int height, int startX, int startY) {
		for (int x = startX; x < width + startX; x++) {
			for (int y = startY; y < height + startY; y++) {
				this.set(x, y, new Way(x, y));
			}
		}
	}
	
	public void setBobSpawn(Vector2 pos) {
		this.bobSpawn = pos;
	}
	
	public Vector2 getBobSpawn() {
		return this.bobSpawn;
	}
}
