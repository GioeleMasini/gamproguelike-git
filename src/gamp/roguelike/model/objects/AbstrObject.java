package gamp.roguelike.model.objects;

import com.badlogic.gdx.math.Vector2;

public abstract class AbstrObject implements IObject {
	private Vector2 position;
	private boolean isMovable;
	private boolean isInvulnerable;
	private int HP;
	
	public AbstrObject() {
		this.isMovable = false;
		this.isInvulnerable = true;
		this.HP = 1; 
	}

	@Override
	public Vector2 getPosition() {
		return this.position;
	}

	@Override
	public void setPosition(final Vector2 p) {
		this.position = p;
	}
	
	public void setMovable(final boolean b) {
		this.isMovable = b;
	}
	
	public boolean isMovable() {
		return this.isMovable;
	}
	
	public boolean isInvulnerable() {
		return this.isInvulnerable;
	}
	
	public void setInvulnerable(boolean b) {
		this.isInvulnerable = b;
	}
	
	public void setHP(int value) {
		this.HP = value;
	}
	
	public int getHP() {
		return this.HP;
	}
	
	public boolean isDead() {
		if (this.HP <= 0) {
			return true;
		}
		return false;
	}
}
