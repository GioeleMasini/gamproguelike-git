package gamp.roguelike.model.objects;

import com.badlogic.gdx.math.Vector2;

public interface IObject {
	
	public Vector2 getPosition();
	
	public void setPosition(final Vector2 p);
	
	public ObjectType getType();
	
	public boolean isMovable();
	
	public boolean isInvulnerable();
	
	public boolean isDead();
	
	public void setHP(int value);
	
	public int getHP();
}
