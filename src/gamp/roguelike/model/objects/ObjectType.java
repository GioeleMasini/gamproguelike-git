package gamp.roguelike.model.objects;

public enum ObjectType {
	HERO, KOBOLD;
	
	public boolean isCreature() {
		if (this == HERO || this == KOBOLD) {
			return true;
		}
		return false;
	}
}
