package gamp.roguelike.model.objects.creatures;

import gamp.roguelike.controller.ModelController;
import gamp.roguelike.model.objects.AbstrObject;

import com.badlogic.gdx.math.Vector2;

public abstract class AbstrCreature extends AbstrObject implements ICreature {
	private State state;
	
	private AbstrCreature() {
		this.setMovable(true);
		this.setInvulnerable(false);
		this.state = State.IDLE;
	}
	
	public AbstrCreature(Vector2 position) {
		this();
		if (position == null) {
			throw new IllegalArgumentException("Creature cannot be created with null position.");
		}
		this.setPosition(position);
	}
	
	public void setPosition(Vector2 p) {
		Double maxX = new Double(ModelController.getInstance().getCurrentMap().getWidth());
		Double maxY = new Double(ModelController.getInstance().getCurrentMap().getHeight());
		if (!(p.x < 0 || p.y < 0 || p.x >= maxX || p.y >= maxY)) {
			super.setPosition(p);	
		}
	}
	
	public State getState() {
		return this.state;
	}
	
	public void setState(State state) {
		this.state = state;
	}

}
