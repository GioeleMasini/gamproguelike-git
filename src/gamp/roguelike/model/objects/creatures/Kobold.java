package gamp.roguelike.model.objects.creatures;

import gamp.roguelike.model.objects.ObjectType;

import com.badlogic.gdx.math.Vector2;


public class Kobold extends AbstrCreature implements ICreature {

	public Kobold(int x, int y) {
		super(new Vector2(x, y));
		this.setHP(3);
	}
	
	public ObjectType getType() {
		return ObjectType.KOBOLD;
	}
}
