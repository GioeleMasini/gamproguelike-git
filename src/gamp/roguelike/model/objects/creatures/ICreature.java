package gamp.roguelike.model.objects.creatures;

import gamp.roguelike.model.objects.IObject;
import gamp.roguelike.model.objects.ObjectType;

public interface ICreature extends IObject {
	
	public boolean isInvulnerable();
	
	public boolean isMovable();
	
	public ObjectType getType();
	
	public State getState();
	
	public void setState(State state);
}
