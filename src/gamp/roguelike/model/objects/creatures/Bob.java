package gamp.roguelike.model.objects.creatures;

import gamp.roguelike.model.objects.ObjectType;

import com.badlogic.gdx.math.Vector2;

public class Bob extends AbstrCreature implements ICreature {
	
	private static Bob SINGLETON;

	private Bob(int x, int y) {
		super(new Vector2(x, y));
	}
	
	public static Bob newBob(int x, int y) {
		Bob.SINGLETON = new Bob(x, y);
		return Bob.SINGLETON;
	}
	
	public static Bob getBob() {
		return Bob.SINGLETON;
	}

	@Override
	public ObjectType getType() {
		return ObjectType.HERO;
	}
}
