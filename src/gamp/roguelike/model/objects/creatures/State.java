package gamp.roguelike.model.objects.creatures;

public enum State {
	MOVING_LEFT, MOVING_RIGHT, MOVING_UP, MOVING_DOWN,
	ATTACKING_LEFT, ATTACKING_RIGHT, ATTACKING_UP, ATTACKING_DOWN,
	IDLE, WAITING;
	
	public boolean isAttacking() {
		return this.name().startsWith("ATTACKING")? true : false;
	}
	
	public boolean isMoving() {
		return this.name().startsWith("MOVING")? true : false;
	}
}
