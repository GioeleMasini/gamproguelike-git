package gamp.roguelike.controller.input;

import gamp.roguelike.GampRoguelike;
import gamp.roguelike.controller.BobController;
import gamp.roguelike.screens.GameScreen;
import gamp.roguelike.screens.Menu2;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;

public class GameInput implements InputProcessor {
	
	GampRoguelike game;
	BobController bob;
	Rectangle[] controls;
	
	// for pinch-to-zoom
	 int numberOfFingers = 0;
	 int fingerOnePointer;
	 int fingerTwoPointer;
	 float lastDistance = 0;
	 Vector3 fingerOne = new Vector3();
	 Vector3 fingerTwo = new Vector3();
	
	public GameInput(BobController bob) {
		this.game = GampRoguelike.getInstance();
		this.bob = bob;
		controls = this.getNewControls();
	}
	
	private Rectangle[] getNewControls() {
		Rectangle[] r = new Rectangle[4];
		r[0] = new Rectangle(Gdx.graphics.getWidth() - 125, 120, 50, 80); 	//top
		r[1] = new Rectangle(Gdx.graphics.getWidth() - 125, 0, 50, 80);		//bot
		r[2] = new Rectangle(Gdx.graphics.getWidth() - 200, 75, 80, 50);	//left
		r[3] = new Rectangle(Gdx.graphics.getWidth() - 80, 75, 80, 50);		//right
		return r;
	}

	@Override
	public boolean keyDown(int keycode) {
		if (keycode == Keys.W) {
			bob.moveUp();
			return true;
		}
		if (keycode == Keys.S) {
			bob.moveDown();
			return true;
		}
		if (keycode == Keys.A) {
			bob.moveLeft();
			return true;
		}
		if (keycode == Keys.D) {
			bob.moveRight();
			return true;
		}
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		if (keycode == Keys.BACK || keycode == Keys.ESCAPE) { 
			this.game.setScreen(new Menu2(game));
			return true;
		}
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		if (character == '+')
			GameScreen.getInstance().addZoom(1f);
		if (character == '-')
			GameScreen.getInstance().addZoom(-1f);
		return false;
	}

	@Override
	public boolean touchDown(int x, int y, int pointer, int button) {
		// for pinch-to-zoom
		 numberOfFingers++;
		 if(numberOfFingers == 1)
		 {
		        fingerOnePointer = pointer;
		        fingerOne.set(x, y, 0);
		 }
		 else if (numberOfFingers == 2)
		 {
		        fingerTwoPointer = pointer;
		        fingerTwo.set(x, y, 0);
		 
		       float distance = fingerOne.dst(fingerTwo);
		        lastDistance = distance;
		 }
		return false;
	}

	@Override
	public boolean touchUp(int x, int y, int pointer, int button) {
		// for pinch-to-zoom           
		/* if(numberOfFingers == 1)
		 {
		        Vector3 touchPoint = new Vector3(x, y, 0);
		        GameScreen.getInstance().getCamera().unproject(touchPoint);
		 }*/
		 numberOfFingers--;
		 
		// just some error prevention... clamping number of fingers (ouch! :-)
		 if(numberOfFingers<0){
		        numberOfFingers = 0;
		 }

		lastDistance = 0;
		
		if (this.lastTouchDown == null) {
			Integer tempY = Gdx.graphics.getHeight() - y;
			if (controls[0].contains(x, tempY)) {
				bob.moveUp();
			} else if (controls[1].contains(x, tempY)) {
				bob.moveDown();
			} else if (controls[2].contains(x, tempY)) {
				bob.moveLeft();
			} else if (controls[3].contains(x, tempY)) {
				bob.moveRight();
			}
		} else {
			this.lastTouchDown = null;
		}
		return false;
	}

	@Override
	public boolean touchDragged(int x, int y, int pointer) {
		// for pinch-to-zoom
		if (this.numberOfFingers == 2) {
			 if (pointer == fingerOnePointer) {
			        fingerOne.set(x, y, 0);
			 }
			 if (pointer == fingerTwoPointer) {
			        fingerTwo.set(x, y, 0);
			 }
			 
			float distance = fingerOne.dst(fingerTwo);
			//float factor = distance / lastDistance;
			 
	
			if (lastDistance < distance) {
			        GameScreen.getInstance().addZoom(0.5f);// += factor;
			 } else if (lastDistance > distance) {
				 GameScreen.getInstance().addZoom(-0.5f);//cam.fieldOfView -= factor;
			 }

			// clamps field of view to common angles...
			// if (cam.fieldOfView < 10f) cam.fieldOfView = 10f;
			 //if (cam.fieldOfView > 120f) cam.fieldOfView = 120f;
			 
			lastDistance = distance;
		}
		 
		//cam.apply(gl);
		if (this.numberOfFingers < 2) {
			moveCamera(x, y);
		}
		return false;
	}

	@Override
	public boolean touchMoved(int x, int y) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}

	
	Vector3 lastTouchDown = null;

    private void moveCamera(int touchX, int touchY) {
    	if (lastTouchDown == null) {
    		lastTouchDown = new Vector3(touchX, touchY, 0);
    	} else {
	        Vector3 translation = new Vector3(lastTouchDown.x - touchX, lastTouchDown.y - touchY, 0);
	        translation.y = -translation.y;
	        GameScreen.getInstance().moveCamera(translation);
	
	        lastTouchDown = new Vector3(touchX, touchY, 0);
    	}
    }
	
    public Rectangle[] getControls() {
    	return this.controls;
    }
    
    public void pause() {
    	// some error prevention...
    	 numberOfFingers = 0;
    }
}
