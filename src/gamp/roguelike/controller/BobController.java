package gamp.roguelike.controller;

import gamp.roguelike.model.map.Map;
import gamp.roguelike.model.objects.creatures.Bob;

import com.badlogic.gdx.math.Vector2;


public class BobController {
	private Bob bob = null;
	private static BobController SINGLETON = null;
	
	private BobController() {
	}
	
	public static BobController getInstance() {
		if (BobController.SINGLETON == null) {
			SINGLETON = new BobController();
		}
		return SINGLETON;
	}
	
	public void moveUp() {
		this.move(0, 1);
	}
	
	public void moveDown() {
		this.move(0, -1);
		
	}
	
	public void moveLeft() {
		this.move(-1, 0);
		
	}
	
	public void moveRight() {
		this.move(1, 0);
		
	}
	
	private void move(int x, int y) {
		Vector2 startPos = this.getBob().getPosition();
		Vector2 finalPos = new Vector2(startPos.x + x, startPos.y + y);
		GameGod.getInstance().move(this.getBob(), startPos, finalPos);
	}
	
	public void spawnBob(Vector2 pos) {
		if (this.bob == null) {
			this.bob = Bob.newBob(Math.round(pos.x), Math.round(pos.y));
		}
	}
	
	public Bob getBob() {
		return this.bob;
	}
	
	public void attack(Vector2 pos) {
		Map map = ModelController.getInstance().getCurrentMap();
		int x = Math.round(pos.x);
		int y = Math.round(pos.y);
		if (map.isOccupied(x, y)) {
			ModelController.getInstance().destroyObject(map.getObjectAt(x, y));
		}
	}
}
