package gamp.roguelike.controller;

import gamp.roguelike.controller.input.GameInput;
import gamp.roguelike.model.map.Map;
import gamp.roguelike.model.objects.IObject;
import gamp.roguelike.model.objects.creatures.ICreature;
import gamp.roguelike.model.objects.creatures.State;
import gamp.roguelike.model.tiles.TileType;
import gamp.roguelike.screens.GameScreen;

import com.badlogic.gdx.math.Vector2;

public class GameGod {

	private static GameGod SINGLETON = null;
	public static final int ANIMATION_TIME_MS = 200;
	
	private ICreature animatedCreature = null;
	private int animationRemainingTime = -1;
	
	public GameGod() {
	}
	
	public static  GameGod getInstance() {
		if (SINGLETON == null) {
			SINGLETON = new GameGod();
		}
		return SINGLETON;
	}
	
	/**
	 * 
	 * @param attacker
	 * @param defender
	 * @return True if the target is destroyed
	 */
	public boolean attack(IObject attacker, IObject defender) {
		Integer dmg = 1;
		if (defender.isInvulnerable()) {
			return false;	//Nothing to do
		}
		
		State animationState = State.valueOf("ATTACKING_" + this.getDirection(attacker.getPosition(), defender.getPosition()));
		this.setAnimationTimer(attacker, animationState);

		defender.setHP(defender.getHP() - dmg);
		GameScreen.getInstance().addString(defender.getPosition().cpy().add(0, 1), dmg.toString());
		
		if (defender.isDead()) {
			ModelController.getInstance().destroyObject(defender);
			return true;
		}
		return false;
	}
	
	private String getDirection(Vector2 startPos, Vector2 endPos) {
		String dir = null;
		if (startPos.x < endPos.x) {
			dir = "RIGHT";
		}
		if (startPos.x > endPos.x) {
			dir = "LEFT";
		}
		if (startPos.y < endPos.y) {
			dir = "UP";
		}
		if (startPos.y > endPos.y) {
			dir = "DOWN";
		}
		return dir;
	}

	//Return true if the 
	public boolean move(IObject o, Vector2 from, Vector2 to) {
		Map map = ModelController.getInstance().getCurrentMap();
		Integer tempX = Math.round(Math.round(to.x));
		Integer tempY = Math.round(Math.round(to.y));
		if (map.get(tempX, tempY).getType() == TileType.WAY) {
			if (map.isOccupied(Math.round(to.x), Math.round(to.y))) {
				IObject target = map.getObjectAt(Math.round(to.x), Math.round(to.y));
				this.attack(o, target);
			} else {
				o.setPosition(to);
			}
		}
		return true;
	}
	
	private void setAnimationTimer(IObject c, State animationState) {
		if (c.getType().isCreature()) {
			((ICreature) c).setState(animationState);
			this.animatedCreature = ((ICreature) c);
		}
		this.animationRemainingTime = ANIMATION_TIME_MS;
	}
	
	public void startGame(GameScreen screen) {
		//Creating new map and getting bob spawn
		Vector2 spawn = ModelController.getInstance().createMap().getBobSpawn();
		//Spawning bob
		BobController bc = BobController.getInstance();
		bc.spawnBob(spawn);
		screen.setInputController(new GameInput(bc));
	}
	
	//Ci van tutti i cambiamenti che coinvolgono il tempo
	public void update(long delta) {
		if (this.animationRemainingTime < 0 && this.animatedCreature != null) {
			this.animatedCreature.setState(State.IDLE);
			this.animatedCreature = null;
		} else {
			this.animationRemainingTime -= delta;
		}
	}

}
