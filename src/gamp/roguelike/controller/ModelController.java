package gamp.roguelike.controller;

import gamp.roguelike.model.map.Map;
import gamp.roguelike.model.objects.IObject;
import gamp.roguelike.model.objects.creatures.ICreature;
import gamp.roguelike.model.objects.creatures.Kobold;

import java.util.Random;

public class ModelController {
	private static ModelController SINGLETON = null;
	private Map map;
	
	private ModelController() {
	}

	public static ModelController getInstance() {
		if (SINGLETON == null) {
			SINGLETON = new ModelController();
		}
		return SINGLETON;
	}
	
	public Map createMap() {
		this.map = MapCreator.createBasicMap();
		//this.map = MapCreator.createRandomMap();
		return this.map;
	}
	
	public Map getCurrentMap() {
		return this.map;
	}
	
	public ICreature getNewEnemy() {
		int x;
		int y;
		do {
			Random random = new Random();
			x = random.nextInt(map.getWidth());
			y = random.nextInt(map.getHeight());
		} while (map.get(x, y) == null || !map.get(x, y).isWalkable());
		return new Kobold(x, y);
	}
	
	public void destroyObject(IObject o) {
		o.setHP(-1);
		map.destroy(o);
	}
}
