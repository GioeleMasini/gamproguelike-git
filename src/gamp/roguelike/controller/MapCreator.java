package gamp.roguelike.controller;

import gamp.roguelike.model.map.Map;
import gamp.roguelike.model.tiles.ITile;
import gamp.roguelike.model.tiles.TileType;

import java.util.Arrays;
import java.util.Random;

import com.badlogic.gdx.math.Vector2;

public class MapCreator {
	private static final int MAP_WIDTH = 40;
	private static final int MAP_HEIGHT = 40;
	
	public static Map createRandomMap() {
		Map map = new Map(MAP_WIDTH, MAP_HEIGHT);
		int nRooms = 4;
		Random r = new Random();
		int x = 0;
		int y = 0;
		int width = 0;
		int height = 0;
		
		for (int i = 0; i < nRooms; i++) {
			x += r.nextInt(15) + width;
			y += r.nextInt(15) + height;
			width = r.nextInt(5) + 4;
			height = r.nextInt(5) + 4;
			//do {
				if (x + width < map.getWidth() - 1 && y + height < map.getHeight() - 1) {
					map.addRoom(width, height, x, y);
					System.out.println("width" + width + " height" + height + " x" + x + " y" + y);
				}
			//} while (canI && e);
		}
		
		MapCreator.setBobSpawn(map);
		MapCreator.cleanWalls(map);
		return map;
	}
	
	public static Map createBasicMap() {
		int width = 30;
		int height = 20;
		Map map = new Map(width, height);
		
		map.addRoom(width - 2, height - 2, 1, 1);
		map.setBobSpawn(new Vector2(1, 1));
		return map;
	}
	
	private static void cleanWalls(Map map) {
		int nNeighbours = 9;
		int[] xyNeighbours = new int[]{-1, 0, 1};
		
		boolean[][] toBeClean = new boolean[MAP_HEIGHT][MAP_WIDTH];
		for (boolean[] line : toBeClean) {
			Arrays.fill(line, true);
		}
		
		for (int x = 0; x < MAP_WIDTH; x++) {
			for (int y = 0; y < MAP_HEIGHT; y++) {
				
				for (int i = 0; i < nNeighbours && toBeClean[y][x]; i++) {
					int tempX = x + xyNeighbours[i % 3];
					int tempY = y + xyNeighbours[i / 3];
					ITile tempTile = map.get(tempX, tempY);
					if (tempTile != null && tempTile.getType() != TileType.WALL) {
						toBeClean[y][x] = false;
					}
				}
				
				/* ANOTHER CHECK FOR PITFALLS
				if (d) {
					d = false;
					
					for (int i = 0; i < nNeighbours; i++) {
						int tempX = x + xyNeighbours[i % 3];
						int tempY = y + xyNeighbours[i / 3];
						if (n >= 0 && n < LENGTH && !pit[n]) {
							d = true;
							break;
						}
					}
				} */
				
				//set the tile as not discoverable
				//discoverable[i] = d;
			}
		}		
		
		//Setting nulls useless walls
		int x = 0;
		int y = 0;
		for (boolean[] line : toBeClean) {
			x = 0;
			for (boolean b : line) {
				if (b) {
					map.set(x, y, null);
				}
				x++;
			}
			y++;
		}
	}
	
	private static void setBobSpawn(Map map) {
		//Setting bob spawn
		int spawnX;
		int spawnY;
		do {
			spawnX = new Random().nextInt(MAP_WIDTH);
			spawnY = new Random().nextInt(MAP_HEIGHT);
		} while (map.get(spawnX, spawnY).getType() != TileType.WAY);
		map.setBobSpawn(new Vector2(spawnX, spawnY));
	}
}
