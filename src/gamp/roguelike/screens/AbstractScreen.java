package gamp.roguelike.screens;

import gamp.roguelike.GampRoguelike;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

/**
 * The base class for all game screens.
 */
public abstract class AbstractScreen implements Screen {
	protected final GampRoguelike game;
	protected final Stage stage;

	private BitmapFont font;
	private SpriteBatch spriteBatch;
	private Skin skin;

	public AbstractScreen(GampRoguelike game) {
		this.game = game;
		this.spriteBatch = new SpriteBatch();
		this.stage = new Stage(0, 0, true, this.spriteBatch);
	}

	protected String getName() {
		return getClass().getSimpleName();
	}

	public BitmapFont getFont() {
		if (font == null) {
			font = new BitmapFont();
		}
		return font;
	}

	public SpriteBatch getBatch() {
		if (spriteBatch == null) {
			spriteBatch = new SpriteBatch();
		}
		return spriteBatch;
	}

	protected Skin getSkin() {
		if (skin == null) {
			skin = new Skin(Gdx.files.internal("uiskin.json"),
					Gdx.files.internal("uiskin.png"));
		}
		return skin;
	}
	
	public Stage getStage() {
		return this.stage;
	}

	// Screen implementation

	@Override
	public void show() {
		// Gdx.app.log( GampRoguelike.LOG, "Showing screen: " + getName() );

		// set the input processor
		Gdx.input.setInputProcessor(stage);
	}

	@Override
	public void resize(int width, int height) {
		// Gdx.app.log( GampRoguelike.LOG, "Resizing screen: " + getName() + " to: " + width + " x " + height );

		// resize and clear the stage
		stage.setViewport(width, height, true);
		stage.clear();
	}

	@Override
	public void render(float delta) {
		// (1) process the game logic

		// update the actors
		stage.act(delta);

		// (2) draw the result

		// clear the screen with the given RGB color (black)
		Gdx.gl.glClearColor(0f, 0f, 0f, 1f);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		// draw the actors
		stage.draw();
	}

	@Override
	public void hide() {
		// Gdx.app.log( GampRoguelike.LOG, "Hiding screen: " + getName() );

		// dispose the resources by default
		dispose();
	}

	@Override
	public void pause() {
		// Gdx.app.log( GampRoguelike.LOG, "Pausing screen: " + getName() );
	}

	@Override
	public void resume() {
		// Gdx.app.log( GampRoguelike.LOG, "Resuming screen: " + getName() );
	}

	@Override
	public void dispose() {
		// Gdx.app.log( GampRoguelike.LOG, "Disposing screen: " + getName() );
		stage.dispose();
		if (font != null)
			font.dispose();
		if (spriteBatch != null)
			spriteBatch.dispose();
		if (skin != null)
			skin.dispose();
	}
}