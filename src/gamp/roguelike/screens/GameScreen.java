package gamp.roguelike.screens;

import gamp.roguelike.GampRoguelike;
import gamp.roguelike.controller.BobController;
import gamp.roguelike.controller.GameGod;
import gamp.roguelike.controller.ModelController;
import gamp.roguelike.controller.input.GameInput;
import gamp.roguelike.model.objects.creatures.ICreature;
import gamp.roguelike.model.objects.creatures.State;
import gamp.roguelike.model.tiles.ITile;
import gamp.roguelike.model.tiles.TileType;

import java.util.LinkedList;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

public class GameScreen extends AbstractScreen {

	private static GameScreen SINGLETON;
	
	private static final int BASE_ZOOM = 20;
	
	private int width, height;
	private GameInput inputController;
	private BobController bob;
    private SpriteBatch batch;
    private BitmapFont font;
    private Float zoom;
    private OrthographicCamera cam;
    private List<StringOnScreen> stringsOnScreen = new LinkedList<StringOnScreen>();

	private GameScreen(GampRoguelike game) {
		super(game);
		this.bob = BobController.getInstance();
		this.batch = new SpriteBatch();
		this.font = new BitmapFont();
        this.width = Gdx.graphics.getWidth();
        this.height = Gdx.graphics.getHeight();
        this.zoom = 0f;
        this.cam = new OrthographicCamera();
        this.getStage().setCamera(cam);
        GameGod.getInstance().startGame(this);
	}
	
	public static GameScreen createInstance(GampRoguelike game) {
		SINGLETON = new GameScreen(game);
		return getInstance();
	}
	
	public static GameScreen getInstance() {
		return SINGLETON;
	}

	@Override
	public void render(float delta) {	
		super.render(delta);
		
		Float zoom = this.getZoom();
		this.font.setScale((float) ((10 + this.zoom/10) * 0.1));
		
		Gdx.gl.glClearColor(0f, 0f, 0f, 1f);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        this.getNewCamera(cam.position);
        
	    batch.setProjectionMatrix(cam.combined);
		batch.begin();
		//drawing map
		List<List<ITile>> map = ModelController.getInstance().getCurrentMap().getTiles();
		
		//rendering map
		font.setColor(Color.DARK_GRAY);
		Vector2 pos = null;
		for (List<ITile> l : map) {
			for (ITile t : l) {
					if (t != null) {
					pos = this.pointForView(t.getPosition(), zoom);
					String c = null;
					if (t.getType() == TileType.WALL) {
						c = "#";
					} else if (t.getType() == TileType.WAY) {
						c = ".";
					}
					font.draw(batch, c, pos.x, pos.y);
				}
			}
		}
		
		//drawing bob
		font.setColor(Color.RED);
		if (bob.getBob() != null) {
			this.drawAnimatedCreature(batch, bob.getBob());
		}
		
		font.setColor(Color.BLUE);
		for (ICreature c : ModelController.getInstance().getCurrentMap().getEnemies()) {
			pos = this.pointForView(c.getPosition(), zoom);
			String ch = "k";
			font.draw(batch, ch, pos.x, pos.y);
		}
		
		//Drawing strings
		font.setColor(Color.YELLOW);
		this.cleanStringsOnScreen();
		for (StringOnScreen s : this.stringsOnScreen) {
			if (!s.isTimeExceeded())
				pos = this.pointForView(s.position, this.getZoom());
				font.draw(batch, s.string, pos.x, pos.y);
		}
		
		batch.end();
		
		// drawing controls
		ShapeRenderer shapeRenderer = new ShapeRenderer();
		shapeRenderer.begin(ShapeType.Rectangle);
		shapeRenderer.identity();
		shapeRenderer.setColor(Color.BLUE);
		Rectangle[] controls = inputController.getControls();
		for (Rectangle r : controls) {
			//batch.draw(r, r.x, r.y, r.width, r.height);
			shapeRenderer.rect(r.x, r.y, r.width, r.height);
		}
		shapeRenderer.end();
		
		
		GameGod.getInstance().update(Math.round(delta * 1000));
		//renderer.render();
	}
	
	private void cleanStringsOnScreen() {
		for (int i = 0; i < this.stringsOnScreen.size(); i++) {
			StringOnScreen s = this.stringsOnScreen.get(i);
			if (s.isTimeExceeded()) {
				this.stringsOnScreen.remove(i);
				i--;
			}
		}
	}

	//Crea un bordo e gestisce lo zoom
	private Vector2 pointForView(Vector2 p, Float zoom) {
		Vector2 finalP = new Vector2();
		finalP.x = (p.x + 1) * zoom;
		finalP.y = (p.y + 2) * zoom;
		return finalP;
	}

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
		
		this.getNewCamera(cam.position);
		this.width = width;
		this.height = height;
	}

	@Override
	public void show() {
		super.show();
		//world = new World();
		//renderer = new WorldRenderer(world, false);
		//controller = new BobController(world);

		//Enabling back key
		Gdx.input.setCatchBackKey(true);
		Gdx.input.setInputProcessor(this.inputController);
	}

	@Override
	public void hide() {
		super.hide();
		//Gdx.input.setInputProcessor(null);
	}

	@Override
	public void pause() {
		super.pause();
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		super.resume();
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		super.dispose();
		
		//Enabling back key
		Gdx.input.setCatchBackKey(false);
		Gdx.input.setInputProcessor(null);
		batch.dispose();
		font.dispose();
	}
	
	public boolean cameraOutOfLimit( Vector3 position ) {
		/*int mapWidth = Map.getInstance().getTiles().size() * zoom;
		int mapHeight = Map.getInstance().getTiles().get(0).size() * zoom;
		
	    int x_right_limit = Math.round(mapWidth*100);
	    int x_left_limit = Math.round(-mapWidth*100);
	    int y_top_limit = Math.round(mapHeight*100);
	    int y_bottom_limit = Math.round(-mapHeight*100);

	    if (position.x < x_left_limit || position.x > x_right_limit)
	        return true;
	    else if (position.y < y_bottom_limit || position.y > y_top_limit)
	    	return true;
	    else*/
	        return false;
	}
    
    private void getNewCamera(Vector3 position) {
    	Vector3 temp = cam.position;
        cam = new OrthographicCamera();
        this.getStage().setCamera(cam);
	    cam.setToOrtho(false, width, height);
        cam.position.set(temp);
        //cam.position.z = zoom + BASE_ZOOM;
	    cam.update();
    }
	
	public void moveCamera(Vector3 translation) {
		this.cam.position.add(translation);
		this.cam.update();
	}
	
	/**
	 * Value from 5 to 35
	 * @return
	 */
	public Float getZoom() {
		return this.zoom + BASE_ZOOM;
	}
	
	public void addZoom(Float value) {
		zoom += value;
		if (zoom > 15) 	zoom = 15f;
		if (zoom < -15)	zoom = -15f;
	}
	
	public OrthographicCamera getCamera() {
		return this.cam;
	}
	
	public void addString(Vector2 position, String string) {
		this.stringsOnScreen.add(new StringOnScreen(string, position));
	}
	
	public void setInputController(GameInput controller) {
		this.inputController = controller;
	}
	
	private void drawAnimatedCreature(SpriteBatch batch, ICreature c) {
		Vector2 pos = bob.getBob().getPosition().cpy();
		State s = c.getState();
		Float movement = 0.2f;
		if (s.isAttacking()) {
			switch(s) {
			case ATTACKING_DOWN:
				pos.add(0, -movement);
				break;
			case ATTACKING_LEFT:
				pos.add(-movement, 0);
				break;
			case ATTACKING_RIGHT:
				pos.add(movement, 0);
				break;
			case ATTACKING_UP:
				pos.add(0, movement);
				break;
			default:
				break;
			
			}
			pos = this.pointForView(pos, this.getZoom());
			font.draw(batch, "@", pos.x, pos.y);
		} else {
			pos = this.pointForView(pos, this.getZoom());
			font.draw(batch, "@", pos.x, pos.y);
		}
	}
	
	/************************* CLASSES *********************/
	private static class StringOnScreen {
		final String string;
		final long spawnTime;
		final Vector2 position;
		private static final int LIVING_TIME_MS = 300;
		//public static final Vector2 LOG_POSITION;
		
		public StringOnScreen(String string, Vector2 position) {
			this.position = position;
			this.string = string;
			this.spawnTime = System.currentTimeMillis();
		}
		
		public boolean isTimeExceeded() {
			long deadTime = spawnTime + LIVING_TIME_MS;
			return System.currentTimeMillis() > deadTime? true : false;
		}
	}
}
