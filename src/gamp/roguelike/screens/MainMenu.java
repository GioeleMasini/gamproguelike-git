package gamp.roguelike.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ClickListener;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.tablelayout.Table;

public class MainMenu implements Screen {
	
	private final static float CAMERA_WIDTH = 10f;
	private final static float CAMERA_HEIGHT = 7f;
	
	private Integer width;
	private Integer height;
	private float ppuX;
	private float ppuY;

	private OrthographicCamera cam;
	private SpriteBatch spriteBatch;
	private Stage stage;
	private TextureRegion newGame;
	private TextureRegion exit;
	
	public MainMenu(Game game) {
		this.cam = new OrthographicCamera(CAMERA_WIDTH, CAMERA_HEIGHT);
		this.cam.position.set(CAMERA_WIDTH / 2f, CAMERA_HEIGHT / 2f, 0);
		this.cam.update();
		this.spriteBatch = new SpriteBatch();
		this.stage = new Stage(480, 360, false, spriteBatch);
        Gdx.input.setInputProcessor(stage);
        
		}

	@Override
	public void render(float delta) {
		//Screen clearing
		Gdx.gl.glClearColor(0.1f, 0.1f, 0.1f, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		
		//spriteBatch.begin();
			this.drawMenu();
		//spriteBatch.end();
		
        stage.act(Math.min(Gdx.graphics.getDeltaTime(), 1 / 30f));
        stage.draw();
        Table.drawDebug(stage);
	}
	
	private void drawMenu() {
		stage.clear();
		
		Image imgNewGame = new Image(newGame);
		imgNewGame.height = 2 * ppuY;
		imgNewGame.width = 5 * ppuY;
		imgNewGame.x = 1 * ppuX;
		imgNewGame.y = (CAMERA_HEIGHT-3) * ppuY;
		imgNewGame.touchable = true;
		
		Image imgExit = new Image(exit);
		imgExit.height = 2 * ppuY;
		imgExit.width = 5 * ppuY;
		imgExit.x = 1 * ppuX;
		imgExit.y = (CAMERA_HEIGHT-5) * ppuY;
		imgNewGame.touchable = true;
		
		
		stage.addActor(imgNewGame);
		stage.addActor(imgExit);
		imgNewGame.setClickListener(new ClickListener() {

			@Override
			public void click(Actor actor, float x, float y) {
				System.out.println("ciccia");
				spriteBatch.begin();
				spriteBatch.draw(newGame, 1*ppuX, 1*ppuY, 2*ppuX, 2*ppuY);
				spriteBatch.end();
			}
            
        });
		
	}

	@Override
	public void resize(int width, int height) {
		this.width = width;
		this.height = height;
		ppuX = (float)width / CAMERA_WIDTH;
		ppuY = (float)height / CAMERA_HEIGHT;
        stage.setViewport(width, height, false);
	}

	@Override
	public void show() {
		TextureAtlas atlas = new TextureAtlas(Gdx.files.internal("images/textures/mainmenutext.pack"));
		this.newGame = atlas.findRegion("newGame");
		this.exit = atlas.findRegion("exit");
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
        stage.setViewport(width, height, false);
	}

}
