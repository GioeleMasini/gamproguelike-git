package gamp.roguelike;

/*Thanks to G. Steigert, code taken from
 * https://code.google.com/p/steigert-libgdx/source/browse/tags/post-20120322/tyrian-game/src/com/blogspot/steigert/tyrian/GampRoguelike.java */

import gamp.roguelike.screens.Menu2;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.FPSLogger;

public class GampRoguelike extends Game {

	private static GampRoguelike SINGLETON = null;
	
	private GampRoguelike() {
		GampRoguelike.SINGLETON = this;
	}
	
	public static GampRoguelike getInstance() {
		if (GampRoguelike.SINGLETON == null) {
			SINGLETON = new GampRoguelike();
		}
		return SINGLETON;
	}
	
	// constant useful for logging
	public static final String LOG = GampRoguelike.class.getSimpleName();

	// whether we are in development mode
	public static final boolean DEV_MODE = false;

	// a libgdx helper class that logs the current FPS each second
	private FPSLogger fpsLogger;

	// services
	/*
	 * private PreferencesManager preferencesManager;
	 * private ProfileManager profileManager;
	 * private LevelManager levelManager;
	 * private MusicManagermusicManager;
	 * private SoundManager soundManager;
	 */

	@Override
	public void create() {
		Gdx.app.log(GampRoguelike.LOG, "Creating game on " + Gdx.app.getType());

		// create the helper objects
		fpsLogger = new FPSLogger();

		setScreen(new Menu2(this));
	}

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
		Gdx.app.log(GampRoguelike.LOG, "Resizing game to: " + width + " x " + height);

		// show the splash screen when the game is resized for the first time;
		// this approach avoids calling the screen's resize method repeatedly
		/*
		 * if (getScreen() == null) { setScreen(new SplashScreen(this)); }
		 */
	}

	@Override
	public void render() {
		super.render();

		// output the current FPS
		if (DEV_MODE)
			fpsLogger.log();
	}

	@Override
	public void pause() {
		super.pause();
		Gdx.app.log(GampRoguelike.LOG, "Pausing game");

		// persist the profile, because we don't know if the player will come
		// back to the game
		//profileManager.persist();
	}

	@Override
	public void resume() {
		super.resume();
		Gdx.app.log(GampRoguelike.LOG, "Resuming game");
	}

	@Override
	public void setScreen(Screen screen) {
		super.setScreen(screen);
		Gdx.app.log(GampRoguelike.LOG, "Setting screen: " + screen.getClass().getSimpleName());
	}

	@Override
	public void dispose() {
		super.dispose();
		Gdx.app.log(GampRoguelike.LOG, "Disposing game");

		// dipose some services
		// musicManager.dispose();
		// soundManager.dispose();
	}

	/*
	 * private OrthographicCamera camera; private SpriteBatch batch; private
	 * Texture texture; private Sprite sprite;
	 * 
	 * @Override public void create() { float w = Gdx.graphics.getWidth(); float
	 * h = Gdx.graphics.getHeight();
	 * 
	 * camera = new OrthographicCamera(1, h/w); batch = new SpriteBatch();
	 * 
	 * texture = new Texture(Gdx.files.internal("data/libgdx.png"));
	 * texture.setFilter(TextureFilter.Linear, TextureFilter.Linear);
	 * 
	 * TextureRegion region = new TextureRegion(texture, 0, 0, 512, 275);
	 * 
	 * sprite = new Sprite(region); sprite.setSize(0.9f, 0.9f *
	 * sprite.getHeight() / sprite.getWidth());
	 * sprite.setOrigin(sprite.getWidth()/2, sprite.getHeight()/2);
	 * sprite.setPosition(-sprite.getWidth()/2, -sprite.getHeight()/2); }
	 * 
	 * @Override public void dispose() { batch.dispose(); texture.dispose(); }
	 * 
	 * @Override public void render() { Gdx.gl.glClearColor(1, 1, 1, 1);
	 * Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
	 * 
	 * batch.setProjectionMatrix(camera.combined); batch.begin();
	 * sprite.draw(batch); batch.end(); }
	 * 
	 * @Override public void resize(int width, int height) { }
	 * 
	 * @Override public void pause() { }
	 * 
	 * @Override public void resume() { }
	 */
}
